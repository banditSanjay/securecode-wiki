---
title: Doshan Jinde
image:
    url: https://lh3.googleusercontent.com/a-/AOh14Gj7VhEq5OzP-GH2FrtATa8WFPyuNcSPcOq0qHwQ=s96-c-rg-br100
    alt: # About your image
website: # Your Website Link
socials:
    linkedin: https://www.linkedin.com/in/doshanjinde/
    twitter: https://twitter.com/drom71094794
    github:  https://github.com/doshanj
draft: false # Change it to false if you want it published.
# Do not change the below values.
type: contributors
layout: single
# Note: Always use https:// whenever putting up links. For e.g., https://payatu.com
# All the fields above are optional
---

Doshan is a professional experienced Senior Security Consultant in Payatu. Background of Web application testing, API testing and infra audit, Mobile application (iOS and Andriod), DevSecOps end to end integration. actively solving the machines with an opensource community like HTB (dr03), try hackme.
