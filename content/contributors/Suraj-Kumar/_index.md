---
title: Suraj Kumar
image: 
    url: # 
    alt: #
website: # Your Website Link
socials:
    linkedin: https://in.linkedin.com/in/surajkum4r
    twitter: https://twitter.com/surajkum4r
    github: # Your Github Link
draft: false # Change it to false if you want it published.
# Do not change the below values.
type: contributors
layout: single
# Note: Always use https:// whenever putting up links. For e.g., https://payatu.com
# All the fields above are optional
---
Suraj is a Sr. Security Consultant at Payatu. He is specialised in performing pentesting on Web Application, Mobile Application (Android and iOS), Thick Client, Network Infrastructure and Cloud Components. He like to bypass hardening of windows system.
<!-- Your description here! -->
